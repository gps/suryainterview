#!/usr/bin/env python
# -*- coding: utf8 -*-
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import json
import logging
import uuid
import webapp2


class MessageHandler(webapp2.RequestHandler):

    def get(self):
        self.log_region_info()
        logging.info('HTTP method: GET')
        email_id = self.request.headers.get('X-Surya-Email-Id', None)
        if not email_id:
            self.send_client_error('Missing email id')
            return
        logging.info('Email Id: %s', email_id)

        response_id = str(uuid.uuid4())
        logging.info('uuid: %s', response_id)

        self.response.headers['Content-Type'] = 'application/json'
        self.response.write(json.dumps({
            'emailId': email_id,
            'uuid': response_id
        }))

    def post(self):
        self.log_region_info()
        logging.info('HTTP method: POST')
        try:
            body = json.loads(self.request.body)
        except ValueError as e:
            logging.error(e)
            self.send_client_error('Invalid JSON in body')
            return

        email_id = body.get('emailId', None)
        uuid = body.get('uuid', None)
        logging.info('email id: %s', email_id)
        logging.info('uuid: %s', uuid)
        if not email_id:
            self.send_client_error('Missing email id')
            return
        if not uuid:
            self.send_client_error('Missing uuid')

        self.response.headers['Content-Type'] = 'text/plain'
        self.response.write('Success')

    def log_region_info(self):
        logging.info('Country: %s', self.request.headers.get('X-AppEngine-Country', None))
        logging.info('Region: %s', self.request.headers.get('X-AppEngine-Region', None))
        logging.info('City: %s', self.request.headers.get('X-AppEngine-City', None))

    def send_client_error(self, message):
        self.response.headers['Content-Type'] = 'text/plain'
        self.response.status_int = 400
        self.response.write('Bad request - ' + message)


class ListHandler(webapp2.RequestHandler):

    def options(self):
        self.response.headers['Access-Control-Allow-Origin'] = '*'
        self.response.headers['Access-Control-Allow-Methods'] = '*'
        self.response.headers['Access-Control-Allow-Headers'] = '*'
        self.response.headers['Access-Control-Max-Age'] = '31536000'
        self.response.write('')

    def post(self):
        self.log_region_info()
        logging.info('HTTP method: POST')
        try:
            body = json.loads(self.request.body)
        except ValueError as e:
            logging.error(e)
            self.send_client_error('Invalid JSON in body')
            return

        email_id = body.get('emailId', None)
        logging.info('email id: %s', email_id)
        if not email_id:
            self.send_client_error('Missing email address')
            return

        self.response.headers['Content-Type'] = 'application/json'
        self.response.headers['Access-Control-Allow-Origin'] = '*'
        self.response.headers['Access-Control-Allow-Methods'] = '*'

        response = {
            'items': [
                {
                    'imageUrl': 'https://pbs.twimg.com/profile_images/718314968102367232/ypY1GPCQ_400x400.jpg',
                    'emailId': 'narendra@modi.com',
                    'firstName': 'Narendra',
                    'lastName': 'Modi'
                },
                {
                    'imageUrl': 'https://pbs.twimg.com/profile_images/451007105391022080/iu1f7brY.png',
                    'emailId': 'barack@obama.com',
                    'firstName': 'Barack',
                    'lastName': 'Obama'
                },
                {
                    'imageUrl': 'https://pbs.twimg.com/profile_images/1047752972/2010-01-14-angela-merkel_property_poster.jpg',
                    'emailId': 'angela@merkel.com',
                    'firstName': 'Angela',
                    'lastName': 'Merkel'
                },
                {
                    'imageUrl': 'https://pbs.twimg.com/profile_images/2504398687/344204969.jpg',
                    'emailId': 'sachin@tendulkar.com',
                    'firstName': 'Sachin',
                    'lastName': 'Tendulkar'
                },
                {
                    'imageUrl': 'https://pbs.twimg.com/profile_images/789817267567472640/BlpcUEvx_400x400.jpg',
                    'emailId': 'virat@kohli.com',
                    'firstName': 'Virat',
                    'lastName': 'Kohli'
                },
                {
                    'imageUrl': 'https://pbs.twimg.com/profile_images/827562109563260930/sD5wLOw1_400x400.jpg',
                    'emailId': 'sunil@chhetri.com',
                    'firstName': 'Sunil',
                    'lastName': 'Chhetri'
                },
                {
                    'imageUrl': 'https://pbs.twimg.com/profile_images/838562307/IMG00882-20100416-0034.jpg',
                    'emailId': 'salman@khan.com',
                    'firstName': 'Salman',
                    'lastName': 'Khan'
                },
                {
                    'imageUrl': 'https://pbs.twimg.com/profile_images/768780297437450240/FJBOm5n8_400x400.jpg',
                    'emailId': 'amitabh@bachchan.com',
                    'firstName': 'Amitabh',
                    'lastName': 'Bachchan'
                },
                {
                    'imageUrl': 'https://pbs.twimg.com/profile_images/881236265391325184/WffFVMgb_400x400.jpg',
                    'emailId': 'priyanka@chopra.com',
                    'firstName': 'Priyanka',
                    'lastName': 'Chopra'
                },
                {
                    'imageUrl': 'https://pbs.twimg.com/profile_images/928189325568827392/3eFJIePn_400x400.jpg',
                    'emailId': 'mesut@ozil.com',
                    'firstName': 'Mesut',
                    'lastName': u'Özil'
                },
                {
                    'imageUrl': 'https://pbs.twimg.com/profile_images/958744181748445186/OUX1E4gK_400x400.jpg',
                    'emailId': 'pierre@aubameyang.com',
                    'firstName': 'Yo',
                    'lastName': 'Pierre'
                },
                {
                    'imageUrl': 'https://pbs.twimg.com/profile_images/960787280431648768/lSc6wqfD_400x400.jpg',
                    'emailId': 'cristiano@ronaldo.com',
                    'firstName': 'Cristiano',
                    'lastName': 'Ronaldo'
                },
                {
                    'imageUrl': 'https://pbs.twimg.com/profile_images/720767103712645122/6XEBAXLj_400x400.jpg',
                    'emailId': 'taylor@swift.com',
                    'firstName': 'Taylor',
                    'lastName': 'Swift'
                },
                {
                    'imageUrl': 'https://pbs.twimg.com/profile_images/961022056631447552/Ywh6u5UM_400x400.jpg',
                    'emailId': 'ozzy@osbourne.com',
                    'firstName': 'Ozzy',
                    'lastName': 'Osbourne'
                },
                {
                    'imageUrl': 'https://pbs.twimg.com/profile_images/876385108961497088/eQL7OlX3_400x400.jpg',
                    'emailId': 'chris@martin.com',
                    'firstName': 'Chris',
                    'lastName': 'Martin'
                },
                {
                    'imageUrl': 'https://pbs.twimg.com/profile_images/378800000483764274/ebce94fb34c055f3dc238627a576d251.jpeg',
                    'emailId': 'tim@cook.com',
                    'firstName': 'Tim',
                    'lastName': 'Cook'
                },
                {
                    'imageUrl': 'https://pbs.twimg.com/profile_images/755268949017698304/TDhoXK3l_400x400.jpeg',
                    'emailId': 'phil@schiller.com',
                    'firstName': 'Phil',
                    'lastName': 'Schiller'
                },
                {
                    'imageUrl': 'https://pbs.twimg.com/profile_images/543839436761358336/iRi_WbYY.jpeg',
                    'emailId': 'satya@nadella.com',
                    'firstName': 'Satya',
                    'lastName': 'Nadella'
                }
            ]
        }

        self.response.write(json.dumps(response))

    def log_region_info(self):
        logging.info('Country: %s', self.request.headers.get('X-AppEngine-Country', None))
        logging.info('Region: %s', self.request.headers.get('X-AppEngine-Region', None))
        logging.info('City: %s', self.request.headers.get('X-AppEngine-City', None))

    def send_client_error(self, message):
        self.response.headers['Content-Type'] = 'text/plain'
        self.response.status_int = 400
        self.response.write('Bad request - ' + message)


app = webapp2.WSGIApplication([
    ('/list', ListHandler),
    ('/message', MessageHandler)
], debug=False)
