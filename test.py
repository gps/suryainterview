#!/usr/bin/env python

import json
import requests


email_id = 'gps@surya-soft.com'

server_name = 'surya-interview.appspot.com'
# server_name = 'localhost:9080'

r = requests.get('http://{}/message'.format(server_name), headers={'X-Surya-Email-Id': email_id})
print(r.status_code)
print(r.text)

uuid = r.json()['uuid']

r = requests.post('http://{}/message'.format(server_name), data=json.dumps({'emailId': email_id, 'uuid': uuid}))

print(r.status_code)
print(r.text)


r = requests.post('http://{}/list'.format(server_name), data=json.dumps({'emailId': email_id}))

print(r.status_code)
print(r.text)
